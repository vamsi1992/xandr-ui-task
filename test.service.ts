import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams, HttpResponse } from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class TestService {

  private getProviderListUrl:string=environment.getProviderListUrl;
  
  private getSeriesByProvider:string=environment.getSeriesByProvider;
  private getEpisodeBySeries:string=environment.getEpisodeBySeries;

  constructor(private http: HttpClient) { }

  getProviderList():Observable<string[]>{
    return this.http.get<string[]>(this.getProviderListUrl);
  }
  getSeriesList(providerName:string):Observable<string[]>{
    return this.http.get<string[]>(this.getSeriesByProvider+providerName);
  }
  getEpisode(SeriesID:string):Observable<string[]>{
    return this.http.get<string[]>(this.getEpisodeBySeries+SeriesID);
  }
}
