import { Component,OnInit } from '@angular/core';
import { TestService } from 'src/app/test.service'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {
  title = 'xandr-ui';

  provider:string;
  providerList:string[];
  seriesEnabled=true;
  episodeEnabled=true;
  providerEnabled=true;
  episodeList:string[];
  numberList:number[];
  seriesList:any[];
  finalFalg:any=false;
  testAll:any=false;
  series:string;
  epToReturn=[]
  episode:any;
  displayMessage:string;
  warningMessage:string;


  constructor(private testService: TestService){}
  ngOnInit(){
    this.getProviderList();
    // this.disableButton();
  }


  getProviderList(){
    this.providerEnabled = false;
    this.testService.getProviderList().subscribe((d)=>{
      console.log("d", d);
      this.providerList = d;
      })
      this.seriesEnabled=true;

  }
  getSeriesList(){
    this.seriesEnabled=false;
   console.log('series',this.seriesEnabled)
    console.log('inside getSeriesList'  )
    this.testService.getSeriesList(this.provider).subscribe((l)=>{
      console.log("l",l);
    this.seriesList=l;
    this.episodeEnabled = true;
   
    
    })
  }
  getEpisodeList(){
    this.episodeEnabled = false;
    console.log('episode',this.episodeEnabled)
    this.testService.getEpisode(this.series).subscribe(a=>{this.episodeList=a;console.log(a)});
    
  }

  changeEp(){

  }
onClear(){
    this.seriesList=[];
    this.episodeList=[];
    this.getProviderList();
}

onSubmit(){
  if(this.episodeEnabled == true || this.seriesEnabled == true || this.providerEnabled == true){
    this.warningMessage="Please select required fields"
  }
  else{
    this.displayMessage = "Data submitted successfully";
  }
 
  setTimeout(() => { this.displayMessage = ""; this.warningMessage =""}, 1000)
  // this.getProviderList();

}

// disableButton(){
//   if(this.providerList=null){
//     this.buttonDisabled = true;
//   } else{
//     this.buttonDisabled= false;
//   }
// }
}


