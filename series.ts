class Series{
    seriesResourceID:string;
    seriesName:string;
    providerName:string;
    updated:string;

    constructor(seriesResourceID: string,seriesName:string, providerName:string,updated:string) {
        this.seriesResourceID = seriesResourceID;
        this.providerName=providerName;
        this.seriesName=seriesName;
        this.updated=updated;
    }
}
